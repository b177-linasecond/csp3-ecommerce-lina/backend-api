const mongoose = require("mongoose")

const prodSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "Name is required"]
	},
	description : {
		type : String,
		required : [true, "Description is required"]
	},
	price : {
		type : Number,
		required : [true, "Price is required"]
	},
	quantity : {
		type : Number,
		required : [true, "Stocks is required"]
	},
	image : {
		type: String,
        required : true
	},
	category : {
		type : String,
		required : true
	},
	sales : {
		type : Number,
		default : 0
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		default : new Date()
	}
})

module.exports = mongoose.model("Product", prodSchema)