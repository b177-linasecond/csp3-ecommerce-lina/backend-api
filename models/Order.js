const mongoose = require("mongoose")

const orderSchema = new mongoose.Schema({
	totalAmount : {
		type : Number,
		required : [true, "Total amount is required"]
	},
	purchasedOn : {
		type : Date,
		default : new Date()
	},
	userId : {
		type : String,
		required : [true, "User ID is required"]
	},
	name : {
		type : String,
		required : [true, "Product ID is required"]
	},
	price : {
		type : Number,
		required : [true, "Quantity is required"]
	},
	image : {
		type: String,
        required : true
	},
	quantity : {
		type : Number,
		required : [true, "Quantity is required"]
	}
})

module.exports = mongoose.model("Order", orderSchema)