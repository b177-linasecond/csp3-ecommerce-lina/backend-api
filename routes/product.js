const express = require("express")
	  router = express.Router()
	  prodController = require("../controllers/product")
	  auth = require("../auth")

router.post("/products", auth.verify, (req, res) => {
	let data = {
		product : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	prodController.createProduct(data).then(result => res.send(result))
})

router.get("/products", (req, res) => {
	prodController.retrieveActiveProducts().then(result => res.send(result))
})

router.get("/products/:productId", (req, res) => {
	prodController.specificProduct(req.params).then(result => res.send(result))
})

router.put("/products/:productId", auth.verify, (req, res) => {
	let data = {
		product : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	prodController.updateProduct(req.params, data).then(result => res.send(result))
})

router.put("/products/:productId/archive", auth.verify, (req, res) => {
	let data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	prodController.archiveProduct(req.params, data).then(result => res.send(result))
})

router.get("/allProducts", (req, res) => {
	let data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	prodController.getAllProducts(data).then(result => res.send(result))
})

router.get("/Rings", (req, res) => {
	prodController.retrieveRing().then(result => res.send(result))
})

router.get("/Necklaces", (req, res) => {
	prodController.retrieveNecklace().then(result => res.send(result))
})

router.get("/Bracelets", (req, res) => {
	prodController.retrieveBracelet().then(result => res.send(result))
})

router.get("/Earrings", (req, res) => {
	prodController.retrieveEarrings().then(result => res.send(result))
})

router.get("/allRings", (req, res) => {
	let data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	prodController.getAllRing(data).then(result => res.send(result))
})

router.get("/allNecklaces", (req, res) => {
	let data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	prodController.getAllNecklace(data).then(result => res.send(result))
})

router.get("/allBracelets", (req, res) => {
	let data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	prodController.getAllBracelet(data).then(result => res.send(result))
})

router.get("/allEarrings", (req, res) => {
	let data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	prodController.getAllEarrings(data).then(result => res.send(result))
})

router.get("/TopProducts", (req, res) => {
	prodController.retrieveTopProducts().then(result => res.send(result))
})

router.get("/NewProducts", (req, res) => {
	prodController.retrieveNewProducts().then(result => res.send(result))
})

module.exports = router