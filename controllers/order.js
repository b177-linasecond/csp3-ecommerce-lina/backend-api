const Order = require("../models/Order")
	  Product = require("../models/Product")
	  bcrypt = require("bcrypt")
	  auth = require("../auth")

module.exports.order = (reqBody) => {
	if(!reqBody.isAdmin) {
		return Product.findById(reqBody.order.productId).then(result => {
			let newQuantity = result.quantity - reqBody.order.quantity

			if(newQuantity >= 0) {
				let amount = result.price * reqBody.order.quantity

				let newOrder = new Order({
					userId : reqBody.id,
					name : result.name,
					price : result.price,
					quantity : reqBody.order.quantity,
					image : reqBody.order.image,
					totalAmount : amount
				})

				return newOrder.save().then((ord, error) => {
					if(ord) {
						return Product.findById(reqBody.order.productId).then(result => {
							let newSales = result.sales + reqBody.order.quantity

		                    if(newQuantity == 0){
		                    	
		                        const updatedProd = {
		                            quantity : newQuantity,
		                            isActive : false,
		                            sales : newSales
		                        };

		                        return Product.findByIdAndUpdate(reqBody.order.productId, updatedProd).then((res, err) => {
		                            if(err){
		                                return err;
		                            }
		                            else{
		                                return true;
		                            }
		                        })
							}
		                    else {
		                        const updatedProd = {
		                            quantity : newQuantity,
		                            sales : newSales
		                        };

		                        return Product.findByIdAndUpdate(reqBody.order.productId, updatedProd).then((res, err) => {
		                            if(err){
		                                return err;
		                            }
		                            else{
		                                return true;
		                            }
		                        })
		                    }
		                })
					}
					else {
						return false
					}
				})
			}

			else {
				return false
			}
			
		})	
	}
	else {
		return Promise.resolve(false)
	}
}

module.exports.allOrders = (reqBody) => {
	if(reqBody.isAdmin) {
		return Order.find({}).then(result => result)
	}
	else {
		return Promise.resolve(false)
	}
}

module.exports.myOrder = (reqBody) => {
	if(!reqBody.isAdmin) {
		return Order.find({userId : reqBody.id}).then(result => {
			if(result.length > 0) {
				return Order.find({userId : reqBody.id}).then(result => result)
			}
			else {
				return false
			}
		})
	}
	else {
		return false
	}
}