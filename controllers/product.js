const Product = require("../models/Product")
	  bcrypt = require("bcrypt")
	  auth = require("../auth")
	  

module.exports.createProduct = reqBody => {
	if(reqBody.isAdmin) {
		let newProduct = new Product({
			name : reqBody.product.name,
			description : reqBody.product.description,
			price : reqBody.product.price,
			quantity : reqBody.product.quantity,
			image : reqBody.product.image,
			category : reqBody.product.category
		})

		return newProduct.save().then((product, error) => {
			if(product) {
				return (`Product is listed.`)
			}
			else {
				return (`Product not listed.`)
			}
		})	
	}
	else {
		return Promise.resolve(`User is not authorized to list a product.`)
	}
}

module.exports.retrieveActiveProducts = () => {
	return Product.find({isActive : true}).then(result => result)
}

module.exports.specificProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		if(result) {
			return result
		}
		else {
			return Promise.resolve(`Product not found.`)
		}
	})
}

module.exports.updateProduct = (reqParams, reqBody) => {
	if(reqBody.isAdmin) {
		let updatedProd = {
			name : reqBody.product.name,
			description : reqBody.product.description,
			price : reqBody.product.price,
			quantity : reqBody.product.quantity
		}

		return Product.findByIdAndUpdate(reqParams.productId, updatedProd).then((product, error) => {
			if(product) {
				return true
			}
			else {
				return false
			}
		})
	}
	else {
		return Promise.resolve(false)
	}
}

module.exports.getAllProducts = data => {
	if(data.isAdmin === true) {
		return Product.find({}).then(result => result)
	}
	else {
		return false
	}
}

module.exports.archiveProduct = (reqParams, reqBody) => {
	if(reqBody.isAdmin) {

		return Product.findById(reqParams.productId).then(result => {
			if(result.isActive == true){
				let updatedProd = {
					isActive : false
				}

				return Product.findByIdAndUpdate(reqParams.productId, updatedProd).then((product, error) => {
					if(product) {
						return true
					}
					else {
						return false
					}
				})
			}

			else {
				let updatedProd = {
					isActive : true
				}

				return Product.findByIdAndUpdate(reqParams.productId, updatedProd).then((product, error) => {
					if(product) {
						return true
					}
					else {
						return false
					}
				})
			}
		})
	
	}
	else {
		return Promise.resolve(false)
	}
}

module.exports.retrieveRing = () => {
	return Product.find({category : "Ring", isActive : true}).then(result => result)
}

module.exports.retrieveNecklace = () => {
	return Product.find({category : "Necklace", isActive : true}).then(result => result)
}

module.exports.retrieveBracelet = () => {
	return Product.find({category : "Bracelet", isActive : true}).then(result => result)
}

module.exports.retrieveEarrings = () => {
	return Product.find({category : "Earrings", isActive : true}).then(result => result)
}

module.exports.getAllRing = data => {
	if(data.isAdmin === true) {
		return Product.find({category : "Ring"}).then(result => result)
	}
	else {
		return false
	}
}

module.exports.getAllNecklace = data => {
	if(data.isAdmin === true) {
		return Product.find({category : "Necklace"}).then(result => result)
	}
	else {
		return false
	}
}

module.exports.getAllBracelet = data => {
	if(data.isAdmin === true) {
		return Product.find({category : "Bracelet"}).then(result => result)
	}
	else {
		return false
	}
}

module.exports.getAllEarrings = data => {
	if(data.isAdmin === true) {
		return Product.find({category : "Earrings"}).then(result => result)
	}
	else {
		return false
	}
}

module.exports.retrieveNewProducts = () => {
	return Product.find({isActive : true}).sort({ createdOn: -1 }).limit(5).then(result => result)
}

module.exports.retrieveTopProducts = () => {
	return Product.find({isActive : true}).sort({ sales: -1 }).limit(4).then(result => result)
}